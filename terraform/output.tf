output "app-public-ip" {
  value = aws_instance.petclinic-app.public_ip
}

output "db-root-pass" {
  value     = aws_db_instance.petclinic-db.password
  sensitive = true
}

output "db-endpoint" {
  value = aws_db_instance.petclinic-db.endpoint
}
