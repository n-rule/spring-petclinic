# Internet VPC
resource "aws_vpc" "petclinic-vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "petclinic-vpc"
  }
}

# Subnets
resource "aws_subnet" "petclinic-public-1" {
  vpc_id                  = aws_vpc.petclinic-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}a"
  tags = {
    Name = "petclinic-public-1"
  }
}

resource "aws_subnet" "petclinic-private-1" {
  vpc_id                  = aws_vpc.petclinic-vpc.id
  cidr_block              = "10.0.11.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.AWS_REGION}a"

  tags = {
    Name = "petclinic-private-1"
  }
}

resource "aws_subnet" "petclinic-private-2" {
  vpc_id                  = aws_vpc.petclinic-vpc.id
  cidr_block              = "10.0.12.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.AWS_REGION}b"

  tags = {
    Name = "petclinic-private-2"
  }
}


# Internet GW
resource "aws_internet_gateway" "petclinic-gw" {
  vpc_id = aws_vpc.petclinic-vpc.id

  tags = {
    Name = "petclinic-gw"
  }
}

# route tables
resource "aws_route_table" "petclinic-public-route" {
  vpc_id = aws_vpc.petclinic-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.petclinic-gw.id
  }

  tags = {
    Name = "petclinic-public-route"
  }
}

# route associations public
resource "aws_route_table_association" "petclinic-public" {
  subnet_id      = aws_subnet.petclinic-public-1.id
  route_table_id = aws_route_table.petclinic-public-route.id
}


