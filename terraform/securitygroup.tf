resource "aws_security_group" "petclinic-app-sg" {
  vpc_id      = aws_vpc.petclinic-vpc.id
  name        = "petclinic-app-sg"
  description = "security group that allows ssh, http and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "petclinic-app-sg"
  }

}

resource "aws_security_group" "petclinic-mysql-sg" {
  vpc_id      = aws_vpc.petclinic-vpc.id
  name        = "petclinic-mysql-sg"
  description = "allow 3306"
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.petclinic-app-sg.id] # allowing access only for petclinic-app-sg
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  tags = {
    Name = "petclinic-mysql-sg"
  }
}
