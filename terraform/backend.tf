terraform {
  backend "s3" {
    bucket         = "terraform-state-baby"
    key            = "global/s3/terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-state-locks"
    #encrypt        = true
  }
}
