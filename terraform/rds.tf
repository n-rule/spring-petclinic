resource "random_password" "password" {
  length           = 8
  special          = true
  override_special = "*"
}

resource "aws_db_subnet_group" "petclinic-private-group" {
  name        = "petclinic-private-group"
  description = "MYSQL subnet group"
  subnet_ids  = [aws_subnet.petclinic-private-1.id, aws_subnet.petclinic-private-2.id]
}

resource "aws_db_instance" "petclinic-db" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  identifier             = "petclinic-db"
  instance_class         = "db.t3.micro"
  db_name                = "petclinic"
  db_subnet_group_name   = aws_db_subnet_group.petclinic-private-group.id
  username               = "root"
  password               = random_password.password.result
  parameter_group_name   = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.petclinic-mysql-sg.id]
  publicly_accessible    = false
  skip_final_snapshot    = true
  tags = {
    Name = "petclinic-db"
  }
}
