packer {
  required_plugins {
    amazon = {
      version = ">=v0.2.23"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "ami_prefix" {
  type    = string
  default = "petclinic-app"
}

variable "ami_commit" {
  type    = string
}


source "amazon-ebs" "ubuntu" {
  ami_name      = "${var.ami_prefix}-${var.ami_commit}"
  instance_type = "t3.micro"
  region        = "eu-central-1"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

build {
  name    = "petclinic-app"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]
  
  provisioner "file" {
    destination = "/tmp/app"
    source      = "target"
  }


  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      #"echo Installing Redis",
      #"sleep 30",
      "sudo apt-get update",
      "sudo apt-get install openjdk-11-jre-headless -y",
      "sudo mkdir -p /app",
      "sudo cp /tmp/app/*.jar /app/petclinic.jar",
      "sudo cp /tmp/app/*.txt /app",
      "echo LIST /tmp/app",
      "ls /tmp/app",
      "echo LIST /app",
      "ls /app"
      #"echo \"FOO is $FOO\" > example.txt",
    ]
  }
}