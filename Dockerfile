
FROM amazoncorretto:8
LABEL Creator: "rule"
LABEL Name: "Petclinic Application Runner"

COPY target/*.jar /petclinic.jar

EXPOSE 8080
CMD ["java", "-jar", "petclinic.jar"]

